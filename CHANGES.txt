0.1.5
---

- Fixed trailing slash issue by removing it if it exists in configuration
- Added clean command to delete and then upload all files from your project
- Silenced Requests cert warnings since sandboxes will always have warnings

0.1.4
---

- Added multiple configurations in one config file with section_name argument
- Prompt user for password if password is not stored in config for security reasons

0.1.3
---

- Added connection test on startup to make sure connection is working or will fail with an error

0.1.2
---

- Fixed bug that locked files on Windows 7 preventing script from working on consecutive uploads

0.1.1
---

- Bug fixes for url paths on Windows
- Fixed bug not allowing config to be overridden via the command line
- Removed logging statements

0.1
---

-  Initial version