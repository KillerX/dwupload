from __future__ import division
from __future__ import print_function
from multiprocessing.pool import ThreadPool
import os
import sys

from dwupload.handlers import WebDavFileSystem

# Probably not the best way to do this, but hey it needs to be done
# open to investigating an alternative approach
errors = []


def locate_file_paths(webdav_filesystem, path):
    for root, dirs, files in os.walk(path):
        for filename in files:
            file_path = os.path.join(root, filename)
            yield (file_path, webdav_filesystem.remote_path(file_path))


def locate_dir_paths(webdav_filesystem, path):
    for root, dirs, files in os.walk(path):
        for dirname in dirs:
            dir_path = os.path.join(root, dirname)
            yield webdav_filesystem.remote_path(dir_path)


def create_directory(task):
    connection, remote_path = task
    connection.mkcol(remote_path)
    status_code = connection.put(remote_path)
    if 200 < status_code >= 300:
        errors.append("Exception [{}]: {}".format(status_code, remote_path))


def upload_file(task):
    connection, local_path, remote_path = task
    with open(local_path, 'r') as f:
        status_code = connection.put(remote_path, f)
        if 200 < status_code >= 300:
            errors.append("Exception [{}]: {}".format(status_code, remote_path))


def clean(connection, watch_path, pool_size=5):
    webdav_filesystem = WebDavFileSystem(connection.dw_server_path, watch_path)
    pool = ThreadPool(pool_size)
    file_tasks = [(connection, x[0], x[1]) for x in locate_file_paths(webdav_filesystem, watch_path)]
    dir_tasks = [(connection, x) for x in locate_dir_paths(webdav_filesystem, watch_path)]

    sys.stdout.write('\r\nCleaning Files\r\n')

    connection.delete(webdav_filesystem.remote_path(watch_path))
    connection.mkcol(webdav_filesystem.remote_path(watch_path))

    # Create directories and write progress
    for i, _ in enumerate(pool.imap_unordered(create_directory, dir_tasks), 1):
        sys.stderr.write('\rCreating Directories: {0:%}'.format(i / len(dir_tasks)))

    sys.stdout.write('\r\n')

    # Upload files and write progress
    for i, _ in enumerate(pool.imap_unordered(upload_file, file_tasks), 1):
        sys.stderr.write('\rUploading Files: {0:%}'.format(i / len(file_tasks)))

    sys.stdout.write('\r\n')

    for error in errors:
        print(error)