from __future__ import print_function
from watchdog.events import PatternMatchingEventHandler
from dwupload.webdav import DemandwareConnection


class WebDavFileSystem(object):
    def __init__(self, base_remote_path, watch_path):
        self.base_remote_path = base_remote_path
        self.watch_path = watch_path

    def _normalize_path_segments(self, file_path):
        relative_file_path = file_path.replace(self.watch_path, "")
        normalized_path = relative_file_path.replace("\\", "/").replace("//", "/")
        path_segments = normalized_path.split("/")
        return path_segments

    def _normalize_server_path_from_segments(self, path_segments):
        url_normalized_path = "/".join(path_segments)[1:]
        return self.base_remote_path + url_normalized_path

    def remote_path(self, local_path):
        path_segments = self._normalize_path_segments(local_path)
        return self._normalize_server_path_from_segments(path_segments)

    def remote_parent_paths(self, local_path):
        path_segments = self._normalize_path_segments(local_path)
        server_paths = []

        while path_segments:
            server_paths.append(self._normalize_server_path_from_segments(path_segments))
            path_segments.pop()

        return server_paths


class DemandwareWebDavHandler(PatternMatchingEventHandler):
    ignore_patterns = ['*.git/*', '*.svn/*', '*.idea/*', '*.DS_Store', '*.tmp', '*.swp', 'Thumbs.db', '*.project/*']

    def __init__(self, watch_path, hostname, username, password, code_version, *args, **kwargs):
        super(DemandwareWebDavHandler, self).__init__(*args, **kwargs)
        self.watch_path = watch_path
        self.conn = DemandwareConnection(hostname, username, password, code_version)
        self.webdav_filesystem = WebDavFileSystem(self.conn.dw_server_path, watch_path)

    def on_created(self, event):
        server_path = self.webdav_filesystem.remote_path(event.src_path)

        if event.is_directory:
            if 200 <= self.conn.mkcol(server_path) < 300:
                print("[created] {0}".format(server_path))
        else:
            with open(event.src_path, 'r') as f:
                if 200 <= self.conn.put(server_path, f) < 300:
                    print("[created] {0}".format(server_path))

    def on_deleted(self, event):
        server_path = self.webdav_filesystem.remote_path(event.src_path)

        if 200 <= self.conn.delete(server_path) < 300:
            print("[deleted] {0}".format(server_path))

    def on_modified(self, event):
        server_path = self.webdav_filesystem.remote_path(event.src_path)

        if not event.is_directory:
            with open(event.src_path, 'r') as f:
                status_code = self.conn.put(server_path, f)

            if 200 <= status_code < 300:
                print("[modified] {0}".format(server_path))

    def on_moved(self, event):
        server_path = self.webdav_filesystem.remote_path(event.src_path)
        server_destination_path = self.webdav_filesystem.remote_path(event.dest_path)

        if not event.is_directory:
            for parent_server_path in self.webdav_filesystem.remote_parent_paths(event.dest_path):
                status_code = self.conn.mkcol(parent_server_path)
                if 200 <= status_code < 300:
                    print("[created] {0}".format(parent_server_path))
                elif status_code == 405:
                    break

            with open(event.dest_path, 'r') as f:
                if 200 <= self.conn.put(server_destination_path, f) < 300:
                    print("[moved] {0} > {1}".format(server_path, server_destination_path))

            if 200 <= self.conn.delete(server_path) < 300:
                print("[deleted] {0}".format(server_path))
        else:
            if 200 <= self.conn.put(server_destination_path) < 300:
                print("[moved] {0} > {1}".format(server_path, server_destination_path))

            if 200 <= self.conn.delete(server_path) < 300:
                print("[deleted] {0}".format(server_path))
